import Sprite from '../base/sprite'

const screenWidth = window.innerWidth
const screenHeight = window.innerHeight

const BG_IMG_SRC = 'images/loading.png'
const BG_WIDTH = 300
const BG_HEIGHT = 512

/**
 * 游戏背景类
 * 提供update和render函数实现无限滚动的背景功能
 */
export default class BackGround extends Sprite {
    constructor(ctx) {
        super(BG_IMG_SRC, BG_WIDTH, BG_HEIGHT)

        this.render(ctx)

        this.top = 100
    }

    update() {
        this.top -= 0.5
        if (this.top <= -screenHeight/5) {
            this.top = -screenHeight/5;
            return;
        }
    }

    /**
     * 参考canvas.drawImage
     * 改变x,y坐标来实现滑动动画效果
     */
    render(ctx) {
        ctx.drawImage(
            this.img,
            -22,
            this.top,
            this.width,
            this.height,
            0,
            0,
            screenWidth,
            screenHeight
        )
    }
}
